from concurrent import futures
import logging
import grpc
import protoFile_pb2
import protoFile_pb2_grpc
import redis


redis = redis.Redis(
    host='localhost',
    port='6379')

class Reply(protoFile_pb2_grpc.ReplyServicer):

    def ReplyFromRedis(self, request, context):

        if request.name in redis:
            age = (redis.get(request.name)).decode()
            message='Age: '+ age
        else:
            message=('Not found')

        return protoFile_pb2.AgeReply(message=message )


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    protoFile_pb2_grpc.add_ReplyServicer_to_server(Reply(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()

if __name__ == '__main__':
    logging.basicConfig()
    serve()



# cd C:\Users\Pacific\vsCode
# python greeter_server.py