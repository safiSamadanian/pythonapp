from __future__ import print_function
import logging
import grpc
import protoFile_pb2
import protoFile_pb2_grpc


def run():

    with grpc.insecure_channel('localhost:50051') as channel:
        while True:
            print ("Enter your desired name");
            stub = protoFile_pb2_grpc.ReplyStub(channel)
            response = stub.ReplyFromRedis(protoFile_pb2.NameRequest(name=input()))
            print(response.message)
            ans = input('Do you want to continue(y/n) :')
            if ans == 'y':
                continue
            else:
                print('Conncetion lost.')
                break
           

if __name__ == '__main__':
    logging.basicConfig()
    run()
